Hello NordSwitch!

To use this task, please run the following commands:

1. Clone the repo:

```
git clone <project_url>
```

2. Cd into it's directory:

```
cd <dir_path>
```

3. Build the Docker image:

```
docker build -t nordswitch-practical:latest .
```

4. Run the container from the image:

```
docker run -p 1234:1234 [--name nordswitch-practical] [-d] nordswitch-practical:latest
```

5. Finally send a http request to `localhost`. Ie:

- Enter http://localhost into the browser,
- http://localhost into Postman for a `GET` request,
- Or `curl http://localhost` from the terminal.
